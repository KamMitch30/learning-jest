import React from 'react';
import '../CSS_Files/Qualities.css'
import qualityJSON from '../Qualities.json'
import Select from 'react-select';

/**
 * @class Represents the contents displayed on the Qualities page. Qualities
 * affect many stats and actions of a character. They should eventually
 * interact with a characters stats and actions on the action page.
 * Information about qualities can be found on page 71 of the core rulebook
 */
class Qualities extends React.Component{
    render(){
        let page;

        //Handle if a character has not been loaded yet (or does not have skills)
        if(this.props.character === null || typeof this.props.character === 'undefined'){
            page = <p>Load a character file to see their qualities</p>;

        }else if(typeof this.props.character.qualities === 'undefined'){
            page = <p>No qualities found, load a character or add qualities to the character's file</p>;

        } else {
            //If character has qualities, generate the page contents
            page = this.qualitiesPage();
        }

        return(<div>
            <h1 className={'Qualities'}>Qualities</h1>
            {page}
        </div>)
    }

    /**
     * Creates a table for both types of qualities, positive and negative
     * @returns the full page with both types of qualities
     */
    qualitiesPage() {
        let layout;

        layout = <div>
            {this.qualitiesTable('Positive')}
            {this.qualitiesTable('Negative')}
        </div>;

        return layout;

    }

    /**
     * Generates the table which will contain all qualities of the spesificed type
     * @param {*} type is what table is currently being created
     */
    qualitiesTable(type){
        let qualitiesList = this.props.character.qualities[type.toLowerCase()];
        let qualitiesRows = [];

        for(let i = 0; i < qualitiesList.length; i++){
            qualitiesRows.push(this.qualitiesRow(type.toLowerCase(), i));
        }
        
        let plusButton = <button className={'Qualities'} onClick={() => this.addQuality(type.toLowerCase())}>Add Custom Quality</button>;

        let presetButton = this.allQualitiesDropdown(type.toLowerCase());

        return(
            <div>
            <h2 className={'Qualities'}>{type}</h2>
            <table className={'Qualities'}>
                    <tbody>
                    <tr className={'Qualities'}>
                        <th className={'Qualities'}/>
                        <th className={'Qualities'}>Name</th>
                        <th className={'Qualities'}>Rtg.</th>
                        <th className={'Qualities'}>Karma</th>
                        <th className={'Qualities'}>Notes</th>
                        <th className={'Qualities'}>Remove</th>
                    </tr>
                    {qualitiesRows}
                    </tbody>
                </table>
                {plusButton}{presetButton}
            </div>
        );
    }

    /**
     * Generates the preset qualities from the qualities.json file
     * into a 
     * @param {*} type is positive/negative depending on the quality
     */
    allQualitiesDropdown(type){
        const options = [];

        qualityJSON[type].forEach(quality => {
            options.push({
                value: quality,
                label: `${quality.qName}` //TODO add full dice value here?
            });
        });

        return <div className={'QualitiesDrop'}><Select
            options={options}
            onChange={val => this.addPresetQuality(val.value.qName, val.value.karma, val.value.rating, val.value.max, type)}
        /></div>
    }

    /**
     * This method creates the table whcih contains the qualities.
     * If the quality has a 0 it cannot have its rating increased, otherwise
     * the rating can be increased from 1 to its max value
     * @param {*} type is positive/negative depending on the type of quality
     * @param {*} index is the spot in the character file array which is currently being loaded
     */
    qualitiesRow(type, index){
        let quality = this.props.character.qualities[type][index];
        let minusButton = <button className={'RemoveQ'} onClick={() => this.removeQuality(type, index)}><span role={'img'} aria-label={'wastebasket'}>🗑️</span></button>;
        let ratingButtonPlus = <button onClick={() => this.addRating(type, index)}>+</button>;
        let ratingButtonMinus = <button onClick={() => this.removeRating(type, index)}>-</button>;
        if(quality === null || quality === "" || quality === undefined){
            return null;
        } else {
            if(quality.rating === 0){
                return <tr className={'Qualities'} key={index}>
                <td className={'Qualities'}></td>
                <td className={'Qualities'}>{quality.qName}</td>
                <td className={'Qualities'}>{}</td>
                <td className={'Qualities'}>{quality.karma}</td>
                <td className={'Qualities'}>{quality.notes}</td>
                <td className={'Qualities'}>{minusButton}</td>
            </tr>
            } else {
            return <tr className={'Qualities'} key={index}>
                <td className={'Qualities'}>{ratingButtonMinus}{ratingButtonPlus}</td>
                <td className={'Qualities'}>{quality.qName}</td>
                <td className={'Qualities'}>{quality.rating}</td>
                <td className={'Qualities'}>{quality.karma}</td>
                <td className={'Qualities'}>{quality.notes}</td>
                <td className={'Qualities'}>{minusButton}</td>
            </tr>
            }
        }
    }

    /**
     * Updates the current rating by checking if there is enough karma 
     * and it does not go over its max.
     * @param {*} type is positive/negative depending on the type of quality
     * @param {*} index is the spot in the character file array which is currently being loaded
     */
    addRating(type, index){
        const quality = this.props.character.qualities[type][index];
        let karmaAdjust = parseInt(quality.karma);
        if(quality.rating < quality.max){
            const response = window.confirm("Increasing " + quality.qName + " from " + quality.rating + " to " + (parseInt(quality.rating) + 1) + " will cost " + karmaAdjust + " karma.\n\nIs it OK to upgrade " + quality.qName + "?");
            if(response){
                const check = this.props.adjKarm(karmaAdjust, `Increased rating of ${quality.qName} quality from ` +
                    `${quality.rating} to ${quality.rating + 1}`,"Karma");
                if(check === true){
                    quality.rating = quality.rating + 1;
                } else {
                    alert("Not enough karma");
                }
            }
        } else{
            alert("The quality is already at its max rating");
        }
    }

    /**
     * Updates the current rating by checking if there is enough karma 
     * and it does not go under 1
     * @param {*} type is positive/negative depending on the type of quality
     * @param {*} index is the spot in the character file array which is currently being loaded
     */
    removeRating(type, index){
        const quality = this.props.character.qualities[type][index];

        const response = window.confirm(`Decreasing ${quality.qName} from ${quality.rating} to ${(parseInt(quality.rating) - 1)} will ` +
        `refund ${Math.abs(quality.karma)} karma.` +
        `\n\nReverting qualities is not allowed by game rules, it is only meant ` +
        `to be done if you accidentally increased a quality. Is it OK to revert ${quality.qName}?`);
        if(response){
            if(quality.rating > 1){
                const check = this.props.adjKarm(Math.abs(quality.karma), `Decreased rating of ${quality.qName} ` +
                    `quality from ${quality.rating} to ${quality.rating - 1}`,"Karma");
                if(check === true){
                    quality.rating = quality.rating - 1;
                } else {
                    alert("Not enough karma");
                }
            }
        }
    }

    /**
     * Adds in a preset quality to the character
     * @param {*} qName name of the quality
     * @param {*} karmaAdjust how much karma it will cost
     * @param {*} rating is the current rating level
     * @param {*} max is the max rating of the quality
     * @param {*} type is if it is positive or negative
     */
    addPresetQuality(qName, karmaAdjust, rating, max, type){
        const response = window.confirm("This quality will cost " + karmaAdjust + " karma.");
        if(response){
            const notes = prompt("Enter any notes about the quality", "");
            if(notes !== null) {
                const check = this.props.adjKarm(karmaAdjust, 'Added Quality: ' + qName,"Karma");
                if(check === true){
                    this.props.adjQuality(qName, karmaAdjust, rating, max, notes, type);
                } else {
                    alert("Not enough karma");
                }
            }
        } 
    }

    /**
     * Creates popups which get the needed information to create a new
     * custom quality. Depending on the type of quality it will check that the ammount of
     * karma that is being taken is correct
     * @param {*} type is positive/negative depending on the quality
     */
    addQuality(type){
        const qNameNew = prompt("Enter the name of the quality:", "Addiction, (Moderate BTLs)");
        if (qNameNew === "") {
            alert("Name must be entered");
        } else if(qNameNew !== null) {
            const karmaNew = prompt("Enter the cost of the quality:", "0");
            if (karmaNew === "" || isNaN(karmaNew)) {
                alert("Must have defined karma amount");
            } else if(karmaNew !== null){
                const ratingNew = prompt("Enter the rating:", "0");
                if(ratingNew === "" || isNaN(ratingNew)){
                    alert("Must have a rating with the quality");
                } else if(ratingNew < 0 ){
                    alert("Rating must be greater than 0");
                } else if(ratingNew !== null) {
                    const maxRatingNew = prompt("Enter the max rating:", "0");
                    if(maxRatingNew === "" || isNaN(maxRatingNew)){
                        alert("There must be a max rating");
                    } else if(maxRatingNew < 0 ){
                        alert("Max rating must be greater than 0");
                    } else if(maxRatingNew !== null) {
                        //Positive must be negative karma and negative must be positive karma
                        if((type === "positive" && karmaNew > 0) || (type === "negative" && karmaNew < 0)){
                            if(type === "positive"){
                                alert("Positive qualities must be negative karma");
                            } else {
                                alert("Negative qualities must be positive karma");
                            }
                        } else {
                            const notes = prompt("Enter any notes about the quality", "");
                            if(notes !== null) {
                                const check = this.props.adjKarm(parseInt(karmaNew), 'Added Quality: ' + qNameNew,"Karma");
                                if(check === true){
                                    this.props.adjQuality(qNameNew, karmaNew, parseInt(ratingNew), parseInt(maxRatingNew), notes, type.toLowerCase());
                                } else {
                                    alert("Not enough karma");
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    /**
     * Removes the current quality from the users JSON using the remQuality in app.js
     * @param {*} type is positive/negative depending on the quality
     * @param {*} index is where that quality is in the characters list.
     */
    removeQuality(type, index){
        const karmaNew = prompt("Enter the amount toof removing the quality:", "-5");
        if (karmaNew === "" || isNaN(karmaNew)) {
            alert("Must have a karma amount entered");
        } else if (karmaNew !== null){
            const check = this.props.adjKarm(parseInt(karmaNew), 'Removed quality: ' +
                this.props.character.qualities[type][index].qName + ". (Original karma: " +
                this.props.character.qualities[type][index].karma + ")","Karma");
            if(check === true){
                this.props.remQuality(type, index);
            } else {
                alert("Not enough karma");
            }
            
        }
    }

}

export default Qualities;